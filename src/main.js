import Vue from 'vue'
import App from './App'
import router from './router'
import http from './api/http'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/zh-CN';  //en
import store from './store';
import dataV from '@jiaminghi/data-view';
import VueAMap from 'vue-amap';

Vue.use(ElementUI, { locale, size: 'small', zIndex: 9000 });
Vue.use(dataV);
Vue.use(VueAMap);

VueAMap.initAMapApiLoader({
  key: "f8ffe058b8e6f5b05e8ff43ca4207393",
  plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor'],
  //默认高德sdk版本为 1.4.4
  v: '1.4.4',
  //这里是高德ui组件的版本号
  uiVersion: '1.0'  //添加 uiVersion 的脚本版本号  这个很重要!必须要引入
});

// 按需引入vue-awesome图标
import Icon from 'vue-awesome/components/Icon';
import 'vue-awesome/icons/chart-bar.js';
import 'vue-awesome/icons/chart-area.js';
import 'vue-awesome/icons/chart-pie.js';
import 'vue-awesome/icons/chart-line.js';
import 'vue-awesome/icons/align-left.js';

// 全局注册图标
Vue.component('icon', Icon);

// 适配flex
import '@/common/flexible.js';

// 引入全局css
import './assets/scss/style.scss';

//引入echart
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

//引入http请求封装函数
Vue.prototype.http = http;
Vue.prototype.http.init(vue);

Vue.config.productionTip = false;
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
// new Vue({
//   router,
//   store,
//   render: (h) => h(App),
// }).$mount('#app');