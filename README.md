<div align="center">
    <p align="center">
        <img src="./src/assets/mouse.jpg" height="100" alt="logo"/>
    </p>
</div>

<div align="center"><h1 align="center">Data.Big.Screen</h1></div>
<div align="center"><h3 align="center">数据大屏看板，可用作可视化大屏展示</h3></div>

<div align="center">

[![star](https://gitee.com/suzong/data-big-screen/badge/star.svg?theme=dark)](https://gitee.com/suzong/data-big-screen/stargazers)
[![fork](https://gitee.com/suzong/data-big-screen/badge/fork.svg?theme=dark)](https://gitee.com/suzong/data-big-screen/members)
[![GitHub license](https://img.shields.io/badge/license-Apache2-yellow)](https://gitee.com/suzong/data-big-screen/blob/master/LICENSE)

</div>

### 🍟 介绍

* 基于vue、element-ui、webpack、百度Echart、高德地图vue-amap、datav等技术的大屏数据看板。
* 实现数据动态刷新渲染、屏幕适应、内部图表组件化自由替换、与后台交互等功能模块。
* 项目需要全屏展示（按F11查看完整）。
* 现在以简单的位置区域来命名的文件，可根据自身业务命名。
* 内部封装element表格组件与搜索组件，实现父子组件的调用 值传递。
* 增加高德地图的引入，后期可实现物体轨迹移动等。

```
如果对您有帮助，您可以点右上角 “Star” 收藏一下 ，获取第一时间更新，谢谢！
```

### ⚡ 更新记录

* 更新记录：[点击查看](https://gitee.com/suzong/data-big-screen/commits/master)
* 后期更新：vue存储store、http请求axios[已集成]、ui框架iview、登录Token处理、、、

### 🥞 项目环境

* 脚手架webpack搭建，未使用ESLint管理代码（无法友好的debugger），未安装unit单元测试与e2e测试
* "vue": "^2.6.11" , "axios": "^0.21.1" , "element-ui": "^2.15.1" , "@jiaminghi/data-view": "^2.7.3" , "echarts": "^4.6.0" , "vue-amap": "^0.5.10",
* "webpack": "^3.6.0" , "webpack-dev-server": "^2.9.1",（未使用webpack4.0,因为webpack4.0与webpack-dev-server独立开了，需要单独安装webpack-dev-server，坑多 采坑一两天没有跳出坑，后期空了再升级到webpack4.0）
* Npm：7.7.6、Node：v14.16.0（v12.0.0+也可以哈），npm>= 3.0.0，node>= 6.0.0。

### 🍿 相关链接

1. [Vue 官方文档](https://cn.vuejs.org/v2/guide/)
2. [Element 官方文档](https://element.eleme.cn/#/zh-CN/component/installation)
3. [DataV 官方文档](http://datav.jiaminghi.com/guide/)
4. [Echarts 实例](https://echarts.apache.org/examples/zh/index.html)
5. [Echarts API文档](https://echarts.apache.org/zh/api.html#echarts)
6. [vue-amap Vue2.x与高德组件](https://elemefe.github.io/vue-amap/#/)
7. [vue-amap 高德官方文档](https://lbs.amap.com/)
8. [项目gitee地址](https://gitee.com/suzong/data-big-screen)

### 🍎 启动流程

1. 需要安装：VSCode、npm或yarn
2. 进入根目录，下载依赖，运行npm install或yarn install，再运行npm run dev或yarn run dev
3. 浏览器访问：`http://localhost:4040`（端口可在config文件夹下index.js文件中配置）
4. 个人比较喜欢用yarn，感觉要快一点吧。安装包npm是npm install 包名，yarn是yarn add 包名。
<table>
    <tr>
        <td><img src="https://gitee.com/suzong/data-big-screen/raw/master/doc/img/yarn_install.png"/></td>
        <td><img src="https://gitee.com/suzong/data-big-screen/raw/master/doc/img/yarn_run_dev.png"/></td>
    </tr>
</table>

### 🍄 效果展示

<table>
    <tr>
        <td><img src="https://gitee.com/suzong/data-big-screen/raw/master/doc/img/s1.png"/></td>
        <td><img src="https://gitee.com/suzong/data-big-screen/raw/master/doc/img/s2.png"/></td>
        <td><img src="https://gitee.com/suzong/data-big-screen/raw/master/doc/img/s3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/suzong/data-big-screen/raw/master/doc/img/s4.gif"/></td>
    </tr>
</table>
<!-- ![项目展示](https://images.gitee.com/uploads/images/2020/1208/183608_b893a510_4964818.gif "20201208_221020.gif") -->

### 🍖 详细说明

| 文件                | 作用/功能                                                              |
| ------------------- | --------------------------------------------------------------------- |
| api                 | http.js封装存放目录，封装了Get Post请求等                               |
| assets              | 静态资源目录，放置 logo 与背景图片                                       |
| assets / style.scss | 通用 CSS 文件，全局项目快捷样式调节                                      |
| assets / index.scss | Index 界面的 CSS 文件                                                  |
| common/...          | 全局封装的 ECharts 和 flexible 插件代码（适配屏幕尺寸，可定制化修改）     |
| components/echart   | 所有 echart 图表（按照位置来命名）                                      |
| router              | 路由函数                                                               |
| store               | 存储函数，存储前端保存的Local Storage                                    |
| utils               | 工具函数与 mixins 函数等                                                |
| views/ index.vue    | 项目主结构                                                             |
| views/其余文件       | 界面各个区域组件（按照位置来命名）                                       |
| main.js             | 主目录文件，引入 Echart/DataV 等文件                                    |

### 🥦 封装组件渲染图表
所有的 ECharts 图表都是基于 `common/echart/index.vue` 封装组件创建的，已经对数据和屏幕改动进行了监听，能够动态渲染图表数据和大小。在监听窗口小大的模块，使用了防抖函数来控制更新频率，节约浏览器性能。

项目配置了默认的 ECharts 图表样式，文件地址：`common/echart/theme.json`。

封装的渲染图表组件支持传入以下参数，可根据业务需求自行添加/删除。

参数名称              | 类型      | 作用/功能                      |
| -------------------| --------- | ------------------------------|
| id                 | String    | 唯一 id，渲染图表的节点（非必填，使用了 $el）|
| className          | String    | class样式名称（非必填）                 |
| options            | Object    | ECharts 配置（必填）                   |
| height             | String    | 图表高度（建议填）                    |
| width              | String    | 图表宽度（建议填）                    |

### 🥦 动态渲染图表
动态渲染图表案例为 `components` 目录下各个图表组件，index 文件负责数据获取和处理，chart 文件负责监听和数据渲染。

chart 文件的主要逻辑为：
```html
<template>
  <div>
    <Echart :options="options" id="id" height="height" width="width" ></Echart>
  </div>
</template>

<script>
  // 引入封装组件
import Echart from '@/common/echart'
export default {
  // 定义配置数据
  data(){ return { options: {}}},
  // 声明组件
  components: { Echart},
  // 接收数据
  props: {
    cdata: {
      type: Object,
      default: () => ({})
    },
  },
  // 进行监听，也可以使用 computed 计算属性实现此功能
  watch: {
    cdata: {
      handler (newData) {
        this.options ={
          // 这里编写 ECharts 配置
        }
      },
      // 立即监听
      immediate: true,
      // 深度监听
      deep: true
    }
  }
};
</script>
```
### 🥦 复用图表组件
复用图表组件案例为中间部分的 `任务通过率与任务达标率` 模块，两个图表类似，区别在于颜色和主要渲染数据。只需要传入对应的唯一 id 和样式，然后在复用的组件 `components/echart/center/centerChartRate` 里进行接收并在对应位置赋值即可。

如：在调用处 `views/center.vue` 里去定义好数据并传入组件
```js
//组件调用
<span>今日任务通过率</span>
<centerChart :id="rate[0].id" :tips="rate[0].tips" :colorObj="rate[0].colorData" />

<span>今日任务达标率</span>
<centerChart :id="rate[1].id" :tips="rate[1].tips" :colorObj="rate[1].colorData" />

...
import centerChart from "@/components/echart/center/centerChartRate";

data() {
  return {
    rate: [
      {
        id: "centerRate1",
        tips: 60,
        ...
      },
      {
        id: "centerRate2",
        tips: 40,
        colorData: {
          ...
        }
      }
    ]
  }
}
```
### 🥦 更换边框
边框是使用了 DataV 自带的组件，只需要去 views 目录下去寻找对应的位置去查找并替换就可以，具体的种类请去 DavaV 官网查看
如：
```html
<dv-border-box-1></dv-border-box-1>
<dv-border-box-2></dv-border-box-2>
<dv-border-box-3></dv-border-box-3>
```
### 🥦 更换图表
直接进入 `components/echart` 下的文件修改成你要的 echarts 模样，可以去[echarts 官方社区](https://gallery.echartsjs.com/explore.html#sort=rank~timeframe=all~author=all)里面查看案例。
### 🥦 Mixins解决自适应适配功能
使用 mixins 注入解决了界面大小变动图表自适应适配的功能，函数在 `utils/resizeMixins.js` 中，应用在 `common/echart/index.vue` 的封装渲染组件，主要是对 `this.chart` 进行了功能注入。
### 🥦 屏幕适配
本项目借助了 flexible 插件，通过改变 rem 的值来进行适配，原设计为 1920px。 ，适配区间为：1366px ~ 2560px，本项目有根据实际情况进行源文件的更改，小屏幕（如:宽为 1366px）需要自己舍弃部分动态组件进行适配，如'动态文字变换组件'会影响布局，需要手动换成一般节点，
```js
// flexible文件位置: `common/flexible.js`,修改部分如下
function refreshRem() {
  var width = docEl.getBoundingClientRect().width;
  // 最小1366px，最大适配2560px
  if (width / dpr < 1366) {
    width = 1366 * dpr;
  } else if (width / dpr > 2560) {
    width = 2560 * dpr;
  }
  // 原项目是1920px我设置成24等份，这样1rem就是80px
  var rem = width / 24;
  docEl.style.fontSize = rem + 'px';
  flexible.rem = win.rem = rem;
}
```

### 👀 调用API
* 查看src/api/http.js的封装。

### 💐 其余备注
* 完善保存着，点个Star，以备后用。

### 👉 特别鸣谢
参考项目[vue-big-screen](https://gitee.com/MTrun/big-screen-vue-datav)，站在巨人的肩膀上做二次开发，更适合项目速成。
